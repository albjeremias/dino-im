FROM ubuntu:20.04
ENV DEBIAN_FRONTEND noninteractive
ENV DINO_VERSION debian/0.3.0-3
WORKDIR /dino

RUN apt update 
RUN apt -y install build-essential devscripts
RUN apt -y install --no-install-recommends git-buildpackage
RUN apt -y install cmake ninja-build valac gettext libgee-0.8-dev libsqlite3-dev libgtk-3-dev libgpgme-dev libsoup2.4-dev libgcrypt20-dev libqrencode-dev libgspell-1-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libwebrtc-audio-processing-dev libsrtp2-dev libnice-dev glib-networking gstreamer1.0-plugins-good gstreamer1.0-gtk3 libsignal-protocol-c-dev git
RUN apt -y install libsoup2.4-dev
RUN apt -y install dh-exec libnotify-dev

RUN git clone https://salsa.debian.org/xmpp-team/dino-im.git /dino
RUN git checkout $DINO_VERSION

RUN sed -i 's/libsoup\-3\.0\-dev/libsoup2\.4\-dev/g' debian/control
RUN sed -i 's/debhelper-compat (= 13)/debhelper-compat (= 12)/g' debian/control
RUN debuild -us -uc -b

ENTRYPOINT ["cp /*.deb /volume/"]

# $ docker build . -t build_dino
# $ mkdir volume
# $ docker run -v $(pwd)/volume:/volume -it build_dino
# $ ls volume/
# dino-im-common_0.3.0-3_all.deb  dino-im_0.3.0-3_amd64.deb
